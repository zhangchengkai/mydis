#include "Source.h"

DECODE_RESULT decode(unsigned char raw[]) {
	DECODE_RESULT r = { 0 };

	if (raw[0] == 0x37) {
		strcpy(r.i, "AAA");
		r.n = 1;
		r.a = X86;
		return r;
	}
	else if (raw[0] == 0xD5) {
		if (raw[1] == 0x0A) strcpy(r.i, "AAD");
		else sprintf(r.i, "AAD %d", int8(raw + 1));
		r.n = 2;
		r.a = X86;
		return r;
	}
	else if (raw[0] == 0xD4) {
		if (raw[1] == 0x0A) strcpy(r.i, "AAM");
		else sprintf(r.i, "AAM %d", int8(raw + 1));
		r.n = 2;
		r.a = X86;
		return r;
	}
	else if (raw[0] == 0x3F) {
		strcpy(r.i, "AAS");
		r.n = 1;
		r.a = X86;
		return r;
	}
	else if (raw[0] == 0x14) {
		sprintf(r.i, "ADC AL, %d", int8(raw + 1));
		r.n = 2;
		r.a = BOTH;
		return r;
	}
	else if (raw[0] == 0x66 && raw[1] == 0x15) {
		sprintf(r.i, "ADC AX, %d", int16(raw + 2));
		r.n = 4;
		r.a = BOTH;
		return r;
	}
	else if (raw[0] == 0x15) {
		sprintf(r.i, "ADC EAX, %d", int32(raw + 1));
		r.n = 5;
		r.a = BOTH;
		return r;
	}

	return r;
}

void runTests();

int main() {
	FILE *in = fopen("test.in", "r");
	FILE *out = fopen("test.out", "w");
	runTests(in, out);
	fclose(in);
	fclose(out);
}
