#define _CRT_SECURE_NO_WARNINGS
#include "Source.h"
#include <stdarg.h>

#define MAX_STRING_LEN 128
#define MAX_HEX_LEN 16

void LogInfo(FILE *out, char *inf, ...) {
	va_list params;
	va_start(params, inf);
	vfprintf(out, inf, params);
	va_end(params);
}

void rstrip(char str[], int n) {
	for (int i = 0; i < n; i++)
		if (str[i] == '\n' || str[i] == '\r')
			str[i] = '\0';
}

char *archiStr[] = {
	"BOTH", "X86", "X64"
};

DECODE_RESULT decode(unsigned char raw[]);

void runTests(FILE *in, FILE *out) {
	int IN_SIZE;
	fseek(in, 0, SEEK_END);
	IN_SIZE = ftell(in);
	fseek(in, 0, SEEK_SET);
	
	while (1) {
		char testName[MAX_STRING_LEN];
		unsigned char raw[MAX_HEX_LEN];
		int n = 0;
		unsigned char asm[MAX_STRING_LEN];
		enum ARCHITECTURE arc;
		DECODE_RESULT r;

		do {
			if (ftell(in) == IN_SIZE)return;
			fgets(testName, MAX_STRING_LEN, in);
			rstrip(testName, MAX_STRING_LEN);
		} while (strlen(testName) == 0);

		char hexStr[MAX_STRING_LEN];
		fgets(hexStr, MAX_STRING_LEN, in);
		rstrip(hexStr, MAX_STRING_LEN);
		unsigned hex[MAX_HEX_LEN];
		char *p = hexStr;
		while (sscanf(p, "%x", hex + n) != -1) {
			n++;
			p += 3;
		}
		for (int i = 0; i < MAX_HEX_LEN; i++) raw[i] = hex[i];

		fgets(asm, MAX_STRING_LEN, in);
		rstrip(asm, MAX_STRING_LEN);

		char archStr[MAX_STRING_LEN];
		fgets(archStr, MAX_STRING_LEN, in);
		rstrip(archStr, MAX_STRING_LEN);
		if (!strcmp(archStr, "BOTH")) arc = BOTH;
		else if (!strcmp(archStr, "X86")) arc = X86;
		else if (!strcmp(archStr, "X64")) arc = X64;

		r = decode(raw);
		if (strcmp(r.i, asm))LogInfo(out, "Fail at %s\nr.i = %s\nasm = %s\n\n", testName, r.i, asm);
		if (r.n != n)LogInfo(out, "Fail at %s\nr.n = %d\n  n = %d\n\n", testName, r.n, n);
		if (r.a != arc)LogInfo(out, "Fail at %s\nr.a = %s\narc = %s\n\n", testName, archiStr[r.a], archiStr[arc]);
	}
}