#define _CRT_SECURE_NO_WARNINGS

#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#define MAX_ASM_STATEMENT_LENGTH 128
#define len(arr) (sizeof(arr) / sizeof(arr[0]))

#define int8(x) (*(signed char *)(x))
#define int16(x) (*(short *)(x))
#define int32(x) (*(int *)(x))

enum ARCHITECTURE {
	BOTH, X86, X64
};

typedef struct _DECODE_RESULT {
	char i[MAX_ASM_STATEMENT_LENGTH]; // instruction
	size_t n; // instruction length
	enum ARCHITECTURE a;
} DECODE_RESULT;
